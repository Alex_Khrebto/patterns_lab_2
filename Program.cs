using System;

interface IRequestHandler
{
    void SetNext(IRequestHandler nextHandler);
    void HandleRequest(string requestType);
}

class BaseRequestHandler : IRequestHandler
{
    private IRequestHandler _nextHandler;

    public void SetNext(IRequestHandler nextHandler)
    {
        _nextHandler = nextHandler;
    }

    public virtual void HandleRequest(string requestType)
    {
        _nextHandler?.HandleRequest(requestType);
    }
}

class CreateAccountHandler : BaseRequestHandler
{
    public override void HandleRequest(string requestType)
    {
        if (requestType == "Create account")
        {
            Console.WriteLine("Create account is handled by CreateAccountHandler");
        }
        else
        {
            base.HandleRequest(requestType);
        }
    }
}

class EditAccountHandler : BaseRequestHandler
{
    public override void HandleRequest(string requestType)
    {
        if (requestType == "Edit account")
        {
            Console.WriteLine("Edit account is handled by EditAccountHandler");
        }
        else
        {
            base.HandleRequest(requestType);
        }
    }
}

class DeleteAccountHandler : BaseRequestHandler
{
    public override void HandleRequest(string requestType)
    {
        if (requestType == "Delete account")
        {
            Console.WriteLine("Delete account is handled by DeleteAccountHandler");
        }
        else
        {
            base.HandleRequest(requestType);
        }
    }
}

interface ITaskComponent
{
    void Add(ITaskComponent component);
    void Remove(ITaskComponent component);
    void Execute();
}

class Task : ITaskComponent
{
    private readonly string _name;

    public Task(string name)
    {
        _name = name;
    }

    public void Add(ITaskComponent component)
    {
        throw new InvalidOperationException("Can't add to a task.");
    }

    public void Remove(ITaskComponent component)
    {
        throw new InvalidOperationException("Can't remove from a task.");
    }

    public void Execute()
    {
        Console.WriteLine($"Executing task {_name}");
    }
}

class Module : ITaskComponent
{
    private readonly List<ITaskComponent> _tasks = new List<ITaskComponent>();
    private readonly string _name;

    public Module(string name)
    {
        _name = name;
    }

    public void Add(ITaskComponent component)
    {
        _tasks.Add(component);
    }

    public void Remove(ITaskComponent component)
    {
        _tasks.Remove(component);
    }

    public void Execute()
    {
        Console.WriteLine($"Executing module {_name}");
        foreach (var task in _tasks)
        {
            task.Execute();
        }
    }
}

class Program
{
    static void Main(string[] args)
    {
        // Создание обработчиков запросов для задач
        var createAccountHandler = new CreateAccountHandler();
        var editAccountHandler = new EditAccountHandler();
        var deleteAccountHandler = new DeleteAccountHandler();

        // Установка последовательности обработчиков в цепочке
        createAccountHandler.SetNext(editAccountHandler);
        editAccountHandler.SetNext(deleteAccountHandler);

        // Обработка запросов
        createAccountHandler.HandleRequest("Edit account");
        createAccountHandler.HandleRequest("Delete account");
        createAccountHandler.HandleRequest("Create account");

        // Создание компонентов - листьев и контейнеров
        var createAccountTask = new Task("Create account");
        var editAccountTask = new Task("Edit account");
        var deleteAccountTask = new Task("Delete account");

        var accountsModule = new Module("Accounts");
        accountsModule.Add(createAccountTask);
        accountsModule.Add(editAccountTask);
        accountsModule.Add(deleteAccountTask);

        var createOrderTask = new Task("Create order");
        var editOrderTask = new Task("Edit order");
        var cancelOrderTask = new Task("Cancel order");

        var ordersModule = new Module("Orders");
        ordersModule.Add(createOrderTask);
        ordersModule.Add(editOrderTask);
        ordersModule.Add(cancelOrderTask);

        var root = new Module("Root");
        root.Add(accountsModule);
        root.Add(ordersModule);

        // Выполнение отдельной задачи
        Console.WriteLine("Executing individual task:");
        createAccountTask.Execute();

        // Выполнение модуля
        Console.WriteLine("\nExecuting module:");
        ordersModule.Execute();

        // Выполнение корневого модуля (вся система управления задачами)
        Console.WriteLine("\nExecuting root module:");
        root.Execute();
    }
}
